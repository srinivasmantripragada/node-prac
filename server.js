var express = require('express');
require('./model/connection');
const bodyparser = require('body-parser');
const config= require('./config')
const userRoutes=require('./routes/user.routes');
var app = express()
 
app.get('/', function (req, res) {
  res.send('Hello World')
})
app.use(bodyparser.json());
app.use('/',userRoutes);
 
app.listen(config.PORT,config.HOST,()=>{
  console.log(`server is running on ${config.PORT}:${config.HOST}`);
})