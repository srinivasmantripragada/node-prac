const HOST ="127.0.0.1"
const PORT = 3090
const DBURL='mongodb://localhost:27017/employee'
const RDBURL='mongodb+srv://user:password@srinivas-caj14.mongodb.net/employee?retryWrites=true'
const DBUSER="dbuser"
const DBPASS="dbpass"

module.exports={
    HOST:HOST,
    PORT:PORT,
    DBURL:DBURL,
    DBUSER:DBUSER,
    DBPASS:DBPASS,
    RDBURL:RDBURL
}

