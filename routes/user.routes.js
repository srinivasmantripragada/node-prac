const router =require('express').Router();
const userCtrl =require('../controller/user.controller');

router.route('/product')
.post(userCtrl.addUsers)

router.route('/product/:userId')
.get(userCtrl.getUser)


router.route('/products')
.get(userCtrl.getUsers)

module.exports = router;